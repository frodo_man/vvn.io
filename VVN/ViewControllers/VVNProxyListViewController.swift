//
//  VVNProxyListViewController.swift
//  Potatso
//
//  Created by Xinghou Liu on 30/06/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

let kVVNProxyListCellId = "VVNProxyListCellId"

class VVNProxyListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelUsage: UILabel!
    @IBOutlet weak var labelExpires: UILabel!
    @IBOutlet weak var progressUsage: UIProgressView!
    @IBOutlet weak var viewUsage: UIView!
    
    // a list of proxy to display
    var proxyInDB: [Proxy] = []
    
    init()
    {
        super.init(nibName: "VVNProxyListViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Proxy.list".localized()
        
        self.setupUIs()
        self.loadProxylistLocal()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        
        if self.proxyInDB.count > 0
        {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showUsageInfo()
        
    }
    
    func setupUIs()
    {
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorColor = UIColor.clear
        self.tableView.register(UINib.init(nibName: "VVNProxyCell", bundle: nil), forCellReuseIdentifier: kVVNProxyListCellId )
        self.tableView.contentInset = UIEdgeInsetsMake(8.0, 0, 0, 0)
        
        self.viewUsage.alpha = 0.0
        
        // usage info
        self.labelUsage.textColor = UIColor.vvnBackground()
        self.labelExpires.textColor = UIColor.vvnBackground()
        self.progressUsage.tintColor = UIColor.vvnBackground()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - Data Model
    func loadProxylistLocal()
    {
        self.proxyInDB = defaultRealm.objects(Proxy.self).sorted(byKeyPath: "name").map{$0}
    }
    
    func proxySelected(_ upstreamProxy: Proxy!)
    {
        VVNProxy.setDefaultProxyObject(with: upstreamProxy)
        
        do{
            
            try ConfigurationGroup.changeProxy(forGroupId: CurrentGroupManager.shared.group.uuid,
                                               proxyId: upstreamProxy.uuid)
        }
        catch let error
        {
            print("Failed to save proxy to server \(error)")
        }
    }
    
    //MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(64.0)
    }
    
    @objc func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.proxyInDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: kVVNProxyListCellId) as? VVNProxyCell
        
        if cell == nil
        {
            tableView.register(UINib.init(nibName: "VVNProxyCell", bundle: nil), forCellReuseIdentifier: kVVNProxyListCellId)
            cell = tableView.dequeueReusableCell(withIdentifier: kVVNProxyCellId) as? VVNProxyCell
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /* example:
         Proxy: [
         "obfsparam": "cloudfront.com",
         "method": "chacha20",
         "protocol": "auth_aes128_md5",
         "server": "tw.vvn.io",
         "password": "vvn",
         "server_port": "443",
         "protocolparam": "12418:i3wmkZ8Y",
         "remarks": "VVN-TW",
         "obfs": "tls1.2_ticket_auth"]
         
         */
        
        let oneProxy = self.proxyInDB[indexPath.row]
        if let vvnCell = cell as? VVNProxyCell
        {
            vvnCell.updateUi(withTitle:  oneProxy.name,
                             subTitle:  oneProxy.host)
            
            if let defaultName = VVNProxy.defaultProxyObject()?.name
            {
                if defaultName == oneProxy.name
                {
                     self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.none)
                }
            }
        }
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let oneProxy = self.proxyInDB[indexPath.row]
        self.proxySelected(oneProxy)
    }
    
    //MARK: - Usage
    func showUsageInfo()
    {
        if let usageInfo = VVNUser.usageInfo
        {
            self.labelUsage.text = usageInfo.availableDataToDisplay()
            self.labelExpires.text = usageInfo.expireTimeToDisplay()
            
            let rate = usageInfo.availableDataRate()
            self.progressUsage.setProgress(rate!, animated: false)
            
            UIView.animate(withDuration: 0.2,
                           animations: {
                            self.viewUsage.alpha = 1.0
            }, completion: { (finished) in
                
            })
        }
    }
    
    func hideUsgeInfo()
    {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.viewUsage.alpha = 0.0
        }, completion: { (finished) in
            
        })
    }

}
