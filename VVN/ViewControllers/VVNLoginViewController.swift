//
//  VVNLoginViewController.swift
//  Potatso
//
//  Created by Xinghou.Liu on 13/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit
import SafariServices

class VVNLoginViewController: UIViewController {

    @IBOutlet weak var viewInputHelper: UIView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPwd: UITextField!
    @IBOutlet weak var mainTopConstant: NSLayoutConstraint!
    
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var buttonTerms: UIButton!
    //@IBOutlet weak var buttonLearnMore: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
    @IBOutlet weak var buttonLogin: UIButton!
    
    var keyboardHeight: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textFieldEmail.inputAccessoryView = self.viewInputHelper
        self.textFieldPwd.inputAccessoryView = self.viewInputHelper
    
        self.registerNotifications()
        self.setupButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    deinit
    {
        self.removeNotification()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textFieldEmail.becomeFirstResponder()
    }
    
    //MARK: - Setting up
    func setupButtons()
    {
        self.buttonTerms.setTitle("Terms".localized(), for: .normal)
        self.buttonRegister.setTitle("Register".localized(), for: .normal)
        //self.buttonLearnMore.setTitle("vvn.io".localized(), for: .normal)
        self.buttonCancel.setTitle("Cancel".localized(), for: .normal)
        
        self.textFieldEmail.placeholder = "Email".localized()
        self.textFieldPwd.placeholder = "Password".localized()
        
        self.buttonLogin.setTitle("Login".localized(), for: .normal)
    }
    
    //MARK: - Actions
    @IBAction func buttonRegisterTapped(_ sender: UIButton!)
    {
        self.showWebsite(for: vvnUrlRegister)
    }
    
    @IBAction func buttonTermsTapped(_ sender: UIButton!)
    {
        let termsVC = VVNTermsViewController.init()
        self.present(termsVC, animated: true, completion: nil)
    }
    
    @IBAction func buttonLearnMoreTapped(_ sender: UIButton!)
    {
        //self.showWebsite(for: vvnUrlLearnMore)
    }
    
    @IBAction func buttonCancelTapped(_ sender: UIButton!)
    {
        self.textFieldPwd.resignFirstResponder()
        self.textFieldEmail.resignFirstResponder()
    }
    
    @IBAction func buttonLoginTapped(_ sender: UIButton?)
    {
        /*
        struct StaticVars {
            static var hasShown = false
        }
        
        if StaticVars.hasShown
        {
            self.tryTologin()
            return
        }
        
        self.view.showPrivacyAlert(in: self,
                                   okAction: {
                                    self.tryTologin()
                                    StaticVars.hasShown = true
                                    return
        }) {
            StaticVars.hasShown = true
            return
        }
 */
        self.tryTologin()

    }
    
    //MARK: - Login
    func tryTologin()
    {
        if (self.textFieldPwd.text?.isEmpty)! ||
            (self.textFieldEmail.text?.isEmpty)!
        {
            return
        }
        
        if let email = self.textFieldEmail.text,
            let pwd = self.textFieldPwd.text
        {
            self.view.startWaiting()
            VVNRequest.login(user:  email ,
                             password: pwd,
                             handler: { (error, dicResponse) in
                                
                                DispatchQueue.main.async(execute: {
                                    self.view.stopWaiting()
                                    if error == nil
                                    {
                                        if let token = dicResponse?["token"] as? String
                                        {
                                            NSLog("Login token:  \(token)")
                                        }
                                        self.dismiss(animated: true, completion: {
                                            
                                        })
                                    }
                                    else
                                    {
                                        self.view.showTempInfo(error?.localizedDescription)
                                    }
                                })
                                
                                
            })
        }
        else
        {
            return;
        }
    }
    
    //MARK: - UI
    func showWebsite(for urlText: String!)
    {
        let sfvc = SFSafariViewController.init(url: URL.init(string:  urlText)!)
        self.present(sfvc, animated: true, completion: nil)
    }
    
    func updateUItoShowKeyboard(_ show: Bool )
    {
        var topMargin: CGFloat = 0.0
        let defaultSpace: CGFloat = 8.0
        
        if show
        {
            guard self.keyboardHeight > 100.0 else
            {
                return
            }
            
            // bottom of the textfield password
            let yPwdBottom = self.textFieldPwd.frame.origin.y + self.textFieldPwd.frame.size.height
            let yKeyboard = self.view.frame.size.height - self.keyboardHeight
            
            if yKeyboard < yPwdBottom
            {
                topMargin = (yPwdBottom - yKeyboard + defaultSpace)
            }
            
        }

        
        self.mainTopConstant.constant = -topMargin
        
        UIView.animate(withDuration:  0.2,
                       animations: {
                        self.view.layoutIfNeeded()
                        
        }) { ( finished) in
            
        }
    }
    
    //MARK: - Notification
    func registerNotifications()
    {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(VVNLoginViewController.gotNotification),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(VVNLoginViewController.gotNotification),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    @objc func gotNotification(_ notification: Notification)
    {
        if let userInfo = notification.userInfo
        {
            if let sizeValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue
            {
                let keyboardSize = sizeValue.cgRectValue.size
                self.keyboardHeight = keyboardSize.height
            }
        }
        
        if notification.name == NSNotification.Name.UIKeyboardWillShow
        {
            self.updateUItoShowKeyboard(true)
        }
        else if notification.name == NSNotification.Name.UIKeyboardWillHide
        {
            self.updateUItoShowKeyboard(false)
        }
    }
    
    func removeNotification()
    {
        NotificationCenter.default.removeObserver(self)
    }
}
