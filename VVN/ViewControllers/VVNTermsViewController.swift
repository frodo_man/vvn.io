//
//  VVNTermsViewController.swift
//  Potatso
//
//  Created by Xinghou.Liu on 04/05/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit
import WebKit

let kTermsPDFName = "vvnTerms-simple"
let kPrivacyPDFName = "vvnPrivacy"

class VVNTermsViewController: UIViewController {

    var webView: WKWebView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var segmentTop: UISegmentedControl!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonDecline: UIButton!
    @IBOutlet weak var webViewBottom: NSLayoutConstraint!
    
    var acceptAction: (()->(Void))?
    var declineAction: (()->(Void))?
    
    
    init()
    {
        super.init(nibName: "VVNTermsViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupWebView()
        self.setupTopSegments()
        self.setupButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func setupButtons()
    {
        if VVNUser.termsAccepted()
        {
             self.buttonAccept.setTitle("Cancel".localized(), for: .normal)
        }
        else
        {
            self.buttonAccept.setTitle("Accept Terms & Privacy Policy".localized(), for: .normal)
        }
        
        self.buttonDecline.backgroundColor = UIColor.white
        self.buttonDecline.layer.borderWidth = 1.0
        self.buttonDecline.layer.borderColor = UIColor.red.cgColor
        self.buttonDecline.setTitle("Decline".localized(), for: .normal)
    }
    
    func setupWebView()
    {
        self.webView = WKWebView.init(frame: self.viewContent.bounds)
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.viewContent .addSubview(self.webView)
        self.showPdfFile(kPrivacyPDFName)
        
    }
    
    func setupTopSegments()
    {
        self.view.backgroundColor = UIColor.vvnBackground()
        self.viewTop.backgroundColor = UIColor.vvnBackground()
        
        self.segmentTop.setTitle("Privacy".localized(), forSegmentAt: 0)
        self.segmentTop.setTitle("Terms".localized(), forSegmentAt: 1)
        
        self.segmentTop.selectedSegmentIndex = 0
        
    }
    
    func showPdfFile(_ name: String!)
    {
        let path = Bundle.main.path(forResource: name, ofType: "pdf")
        let fileUrl = URL.init(fileURLWithPath: path!)
        let folderUrl = URL.init(fileURLWithPath: Bundle.main.bundlePath, isDirectory: true)
        
        self.webView.loadFileURL(fileUrl, allowingReadAccessTo: folderUrl)
    }
    
    //MARK: - Actions
    @IBAction func segmentOptionChanged(_ segControl: UISegmentedControl!)
    {
        if segControl.selectedSegmentIndex == 0
        {
            self.showPdfFile(kPrivacyPDFName)
        }
        else
        {
            self.showPdfFile(kTermsPDFName)
        }
    }
    
    @IBAction func buttonAcceptTapped(_ sender: UIButton!)
    {
        if !VVNUser.termsAccepted()
        {
            VVNUser.acceptTerms()
        }
        
        if let action = self.acceptAction
        {
            action()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonDeclineTapped(_ sender: UIButton!)
    {
        VVNUser.declineTerms()
        
        if let action = self.declineAction
        {
            action()
        }
        self.dismiss(animated: true, completion: nil)
    }
}
