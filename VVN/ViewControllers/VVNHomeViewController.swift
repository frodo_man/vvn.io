//
//  VVNHomeViewController.swift
//  Potatso
//
//  Created by Xinghou.Liu on 13/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

let kVVNProxyCellId: String! = "kVVNProxyCellId"

class VVNHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonConnect: UIButton!
    @IBOutlet weak var labelPrivacy: UILabel!
    @IBOutlet weak var viewProxy: UIView!
    @IBOutlet weak var buttonProxy: UIButton!
    @IBOutlet weak var buttonProxyArrow: UIButton!
    
    @IBOutlet weak var switchConnect: UISwitch!
    @IBOutlet weak var viewCircle: UIView!
    
    @IBOutlet weak var labelHowTo: UILabel!
    @IBOutlet weak var labelDataLeft: UILabel!
    @IBOutlet weak var labelExpireInfo: UILabel!
    @IBOutlet weak var viewUsage: UIView!
    
    var viewDidAppeared: Bool = false
    var hasUsageInfo: Bool = false
    
    // a list of proxy from the ssr.vvn.io api
    var proxyList: NSMutableArray! = NSMutableArray.init()
    
    // a list of proxy to display
    var proxyInDB: [Proxy] = []
    
     init()
    {
        super.init(nibName: "VVNHomeViewController", bundle: nil)
        let _ = Manager.sharedManager
        self.addNotifications()
    }
    
    deinit {
         self.removeNotifications()
    }
    
    var status: VPNStatus = .off {
        didSet(o)
        {
            if self.viewDidAppeared
            {
                self.updateConnectButton()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupUIs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewDidLayoutSubviews() {
        if !self.viewDidAppeared
        {
            self.viewCircle.layer.cornerRadius = self.viewCircle.frame.size.width/2.0
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Show T & C, Privacy or Login
        if VVNUser.termsAccepted()
        {
            self.showLoginViewOrProxyList()
        }
        else
        {
            if !self.viewDidAppeared
            {
                weak var weakSelf = self
                self.showTermsPrivacy({
                    weakSelf?.showLoginViewOrProxyList()
                })
            }
            else
            {
                if self.proxyInDB.count == 0
                {
                    self.showLoginViewOrProxyList()
                }
            }
        }
        self.viewDidAppeared = true
        
        self.updateProxyInfoUI()
        self.handleRefreshUI()
    }
    
    //MARK: - UIs
    func confirmLogout()
    {
        //alert
        let alert = UIAlertController(title: nil,
                                      message: "Logout.confirm".localized(),
                                      preferredStyle: .alert)
        // ok action
        let okAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
            VVNRequest.logout { (error) in
                
                if self.status == .on
                {
                    let group = CurrentGroupManager.shared.group
                    VPN.switchVPN(group) { [unowned self] (error) in
                        if let error = error {
                            Alert.show(self, message: "\("Fail to switch VPN.".localized()) (\(error))")
                        }
                        DispatchQueue.main.async(execute: {
                            self.view.stopWaiting()
                            self.showLoginViewController()
                        })
                    }
                }
                else
                {
                    self.showLoginViewController()
                }
            }
        }
        alert.addAction(okAction)
        
        //cancel action
        let cancelAction = UIAlertAction(title: "No".localized(), style: .cancel) { (cAction) in
            alert.dismiss(animated: true, completion: {
                
            })
        }
        alert.addAction(cancelAction)
       
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showLoginViewOrProxyList()
    {
        self.status = Manager.sharedManager.vpnStatus
        if let _ = VVNUser.userToken()
        {
            if self.proxyInDB.count == 0
            {
                self.loadProxyListLive()
            }
            
            // get the data usage info
            self.getUsageInfo()
        }
        else
        {
            self.showLoginViewController()
        }
    }
    
    func getUsageInfo()
    {
        if self.status == .on
        {
            return
        }
        
        DispatchQueue.main.async(execute: {
            self.viewCircle.startWaiting()
        })
        
        VVNRequest.getUsage(handler: { (error , usageInfo) in
            DispatchQueue.main.async(execute: {
                
                self.viewCircle.stopWaiting()
                
                if error != nil
                {
                    self.hasUsageInfo = false
                    return;
                }
                
                if let _ = usageInfo
                {
                    self.showUsageInfo()
                    self.hasUsageInfo = true
                }
            })
        })
    }
    
    func setupUIs()
    {
        self.title = "VVN"
        
        self.labelPrivacy.text = "VPN.privacy".localized()
        self.labelPrivacy.textColor = UIColor.vvnBackground()
        self.labelPrivacy.numberOfLines = 1
        
        self.buttonConnect.backgroundColor = UIColor.vvnBackground()
        self.buttonConnect.setTitleColor(UIColor.vvnText(), for: .normal)
        
        // Right bar button
        let reflesh = UIBarButtonItem.init(title: "Logout".localized(),
                                           style: .plain,
                                           target: self,
                                           action: #selector(VVNHomeViewController.buttonReloadTapped))
        reflesh.tintColor = UIColor.vvnText()
        
        
        self.navigationItem.rightBarButtonItem = reflesh
        
        let termsBn = UIBarButtonItem.init(title: "Terms".localized(),
                                           style: .plain,
                                           target: self,
                                           action: #selector(VVNHomeViewController.buttonTermsTapped))
        termsBn.tintColor = UIColor.vvnText()
        self.navigationItem.leftBarButtonItem = termsBn
        
        //proxy button
        self.viewProxy.layer.cornerRadius = 25.0
        self.viewProxy.layer.borderColor = UIColor.vvnBackground().cgColor
        self.viewProxy.layer.borderWidth = 1.0
        self.viewProxy.clipsToBounds = true
        
        self.buttonProxy.tintColor = UIColor.vvnBackground()
        self.buttonProxyArrow.tintColor = UIColor.vvnBackground()
        
        //Switch circle
        self.viewCircle.clipsToBounds = true
        self.viewCircle.layer.borderWidth = 1.0
        self.viewCircle.addShadow(with: UIColor.gray)
        self.viewCircle.layer.borderColor = UIColor.gray.cgColor
        
        self.switchConnect.onTintColor = UIColor.vvnBackground()
        self.switchConnect.backgroundColor = UIColor.gray
        self.switchConnect.layer.cornerRadius = 15.5
        self.switchConnect.clipsToBounds = true
        

        //Usage Info
        self.setupUsageInfo()
    }
    
    func setupUsageInfo()
    {
        self.labelHowTo.text = "Home.howto".localized()
        
        self.labelHowTo.textColor = UIColor.vvnBackground()
        self.labelDataLeft.textColor = UIColor.vvnBackground()
        self.labelExpireInfo.textColor = UIColor.vvnBackground()
        
        self.hideUsageInfo()
    }
    
    func updateConnectButton()
    {
        DispatchQueue.main.async {
            self.buttonConnect.isEnabled = [VPNStatus.on, VPNStatus.off].contains(self.status)
            
            switch self.status
            {
            case .connecting, .disconnecting:
                self.buttonConnect.isEnabled = false
                self.view.startWaiting()
            default:
                self.buttonConnect.setTitle(self.status.hintDescription, for: .normal)
                self.buttonConnect.isEnabled = true
                self.view.stopWaiting()
            }
            self.buttonConnect.backgroundColor = self.status.color
            self.buttonConnect.setTitle(self.status.hintDescription, for: .normal)
            
            if (self.status == .on && !self.switchConnect.isOn) ||
               (self.status == .off && self.switchConnect.isOn)
            {
                self.switchConnect.setOn((self.status == .on), animated: false)
            }
            
        }
    }
    
    func handleRefreshUI() {

        self.status = Manager.sharedManager.vpnStatus
        
        DispatchQueue.main.async {
            if(self.status == .on)
            {
                self.labelHowTo.text = "Home.howto.disconnect".localized()
                self.updateCircleView(with: UIColor.vvnBackground())
            }
            else
            {
                self.labelHowTo.text = "Home.howto".localized()
                self.updateCircleView(with: UIColor.gray)
            }
        }
    }
    
    func updateCircleView(with color: UIColor!)
    {
        self.viewCircle.layer.borderColor = color.cgColor
        self.viewCircle.addShadow(with:  color)
    }
    
    func showLoginViewController()
    {
        let loginVC = VVNLoginViewController.init(nibName: "VVNLoginViewController", bundle: nil)
        self.present( loginVC, animated: true) {
            
        }
    }
    
    func showTermsPrivacy(_ acceptAction: (()->Void)? )
    {
        let termsVC = VVNTermsViewController.init()
        termsVC.acceptAction = acceptAction;
        
        weak var weakSelf = self
        termsVC.declineAction = { ()->Void in
            if weakSelf?.status == .on
            {
                weakSelf?.status = .disconnecting
                weakSelf?.connectOrDisconnectVPN()
            }
        }
        
        self.present(termsVC, animated: true, completion: nil)
    }
    
    func updateProxyInfoUI()
    {
        DispatchQueue.main.async(execute: {
            if let proxy = VVNProxy.defaultProxyObject()
            {
                let nameLabel = "Proxy.current".localized() + ": " + proxy.name
                self.buttonProxy.setTitle(nameLabel, for: .normal)
            }
        })
    }
    
    func showUsageInfo()
    {
        self.viewUsage.isHidden = false
        
        if let usageInfo = VVNUser.usageInfo
        {
            self.labelDataLeft.text = usageInfo.availableDataShort()
            self.labelExpireInfo.text = usageInfo.expireTimeShort()
        }
    }
    
    func hideUsageInfo()
    {
        self.viewUsage.isHidden = true
    }
    
    //MARK: - Data
    
    func setDefaultProxyFirstTime()
    {
        if self.proxyInDB.count > 0
        {
            self.proxySelected(self.proxyInDB[0])
        }
    }
    
    func setDefaultProxy(_ upstreamProxy: Proxy!)
    {
        //if ConfigurationGroup
    }
    
    func proxySelected(_ upstreamProxy: Proxy!)
    {
        do{
            
            try ConfigurationGroup.changeProxy(forGroupId: CurrentGroupManager.shared.group.uuid,
                                               proxyId: upstreamProxy.uuid)
        }
        catch let error
        {
            print("Failed to save proxy to server \(error)")
        }
    }
    
    func loadProxylistLocal()
    {
        self.proxyInDB = defaultRealm.objects(Proxy.self).sorted(byKeyPath: "name").map{$0}
        
        if self.proxyInDB.count > 0
        {
            self.updateProxyInfoUI()
        }
    }
    
    func loadProxyListLive()
    {
        self.view.startWaiting()
        
        VVNRequest.getProxies(handler: { (pError, proxies) in
            
            DispatchQueue.main.async(execute: {
                self.view.stopWaiting()
                
                if pError != nil
                {
                    if self.proxyInDB.count == 0 && VVNUser.userToken() == nil
                    {
                        self.showLoginViewController()
                    }
                    self.view.showTempInfo(pError?.localizedDescription)
                    return
                }
                
                if pError == nil && proxies != nil
                {
                    // remove all from the data base
                    VVNProxy.deleteAllProxiesInDB()
                    self.proxyInDB = []

                    VVNProxy.saveAll(proxies: proxies!)
                    
                    self.proxyInDB = defaultRealm.objects(Proxy.self).sorted(byKeyPath: "name").map{$0}
                  
                    self.updateProxyInfoUI()
                }
                else if proxies == nil || proxies?.count == 0
                {
                    self.loadProxylistLocal()
                }
            })
        })
    }
    
    //MARK: - UITableViewDataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(96.0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.proxyInDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: kVVNProxyCellId) as? VVNProxyCell
        
        if cell == nil
        {
            tableView.register(UINib.init(nibName: "VVNProxyCell", bundle: nil), forCellReuseIdentifier: kVVNProxyCellId)
            cell = tableView.dequeueReusableCell(withIdentifier: kVVNProxyCellId) as? VVNProxyCell
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /* example:
         Proxy: [
         "obfsparam": "cloudfront.com",
         "method": "chacha20",
         "protocol": "auth_aes128_md5",
         "server": "tw.vvn.io",
         "password": "vvn",
         "server_port": "443",
         "protocolparam": "12418:i3wmkZ8Y",
         "remarks": "VVN-TW",
         "obfs": "tls1.2_ticket_auth"]
         
         */
        
        let oneProxy = self.proxyInDB[indexPath.row]
        if let vvnCell = cell as? VVNProxyCell
        {
            vvnCell.updateUi(withTitle:  oneProxy.name,
                             subTitle:  oneProxy.host)
        }
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let oneProxy = self.proxyInDB[indexPath.row]
        self.proxySelected(oneProxy)
        
    }
    
    //MARK: - Actions
    @IBAction func switchConnectValueChanged(_ sender: UISwitch!)
    {
        if self.switchConnect.isOn
        {
            if self.status != .on
            {
                self.tryToChangeVPNStatus()
            }
        }
        else if self.status != .off
        {
            self.tryToChangeVPNStatus()
        }
    }
    
    @IBAction func switchConnect(tapped sender: UISwitch!)
    {
        
    }
    
    func tryToChangeVPNStatus()
    {
        if !self.hasUsageInfo
        {
            self.getUsageInfo()
            self.switchConnect.setOn(false, animated: false)
            return
        }
        
        if let userInfo = VVNUser.usageInfo
        {
            if !userInfo.validToUse
            {
                self.view.showTempInfo(VVNUser.usageInfo?.errorMessage)
                self.switchConnect.setOn(false, animated: false)
                return;
            }
        }
        
        if VVNUser.termsAccepted()
        {
            self.changeVPNStatus()
        }
        else
        {
            weak var weakSelf = self;
            self.showTermsPrivacy({
                weakSelf?.changeVPNStatus()
            })
        }
    }
    
    @IBAction func buttonConnect(tapped sender: UIButton!)
    {
        if self.status != .on
        {
        }
        
        if VVNUser.termsAccepted()
        {
            self.changeVPNStatus()
        }
        else
        {
            weak var weakSelf = self;
            self.showTermsPrivacy({
                 weakSelf?.changeVPNStatus()
            })
        }
    }
    
    func changeVPNStatus()
    {
        if self.status == .on
        {
            self.status = .disconnecting
        }
        else
        {
            self.status = .connecting
        }
        
        self.connectOrDisconnectVPN()
    }
    
    func connectOrDisconnectVPN()
    {
        let group = CurrentGroupManager.shared.group
        VPN.switchVPN(group) { [unowned self] (error) in
            if let error = error {
                Alert.show(self, message: "\("Fail to switch VPN.".localized()) (\(error))")
            }
            DispatchQueue.main.async(execute: {
                self.view.stopWaiting()
            })
        }
    }
    
    
    @objc @IBAction func buttonReloadTapped( _ sender: UIBarButtonItem!)
    {
        // call this to reload proxy list
        // self.loadProxyList()
        
        // logout
        self.confirmLogout()
    }
    
    @objc func buttonTermsTapped(_ sender: UIBarButtonItem!)
    {
        self.showTermsPrivacy {
        
        }
    }
    
    @IBAction func buttonProxyTapped(_ sender: UIButton!)
    {
        if self.status == .on
        {
            return
        }
        
        let proxyListVC = VVNProxyListViewController()
        self.navigationController?.pushViewController(proxyListVC, animated: true)
    }
    
    //MARK: - Notification
    func addNotifications()
    {
        NotificationCenter.default.addObserver(self,
                                               selector:#selector(gotNotification),
                                               name: NSNotification.Name(rawValue: kProxyServiceVPNStatusNotification),
                                               object: nil)
        
        CurrentGroupManager.shared.onChange = { group in
            self.handleRefreshUI()
        }
    }
    
    @objc func gotNotification(_ notification: NSNotification!)
    {
        self.handleRefreshUI()
    }
    
    func removeNotifications()
    {
        NotificationCenter.default.removeObserver(self)
    }
}
