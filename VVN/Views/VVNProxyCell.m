//
//  VVNProxyCell.m
//  Potatso
//
//  Created by Xinghou Liu on 20/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

#import "VVNProxyCell.h"
#import "Potatso-Swift.h"

@implementation VVNProxyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setupUIs];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected)
    {
        self.viewMain.backgroundColor = [[UIColor vvnBackground] colorWithAlphaComponent: 0.3f];
        self.imageViewTick.hidden = NO;
    }
    else
    {
        self.viewMain.backgroundColor = [UIColor clearColor];
        self.imageViewTick.hidden = YES;
    }
    
}


-(void)setupUIs
{
    self.imageViewTick.tintColor =
    self.labelTitle.textColor = [UIColor vvnBackground];
    
    self.labelSubtitle.textColor = [UIColor grayColor];
    
    self.viewMain.layer.cornerRadius = 25.0f;
    self.viewMain.layer.borderWidth = 0.6f;
    self.viewMain.layer.borderColor = [UIColor vvnBackground].CGColor;
    
    self.backgroundColor = [UIColor clearColor];
    
    self.imageViewTick.hidden = YES;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)updateUiWithTitle: (NSString*) titleText
                subTitle: (NSString*)subText
{
    self.labelTitle.text = titleText;
    self.labelSubtitle.text = subText;
    self.labelSubtitle.hidden = YES;
}


@end
