//
//  VVNTableViewCell.m
//  Potatso
//
//  Created by Xinghou.Liu on 16/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

#import "VVNTableViewCell.h"

@implementation VVNTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected)
    {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
