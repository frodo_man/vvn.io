//
//  UIView+VVNStyle.h
//  Potatso
//
//  Created by Xinghou.Liu on 16/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (VVNStyle)

-(void)startWaiting;
-(void)startWaitingSpinnerOnly;
-(void)stopWaiting;
-(void)showTempInfo: (NSString* __nullable)infoText;

-(void)showPrivacyAlertIn: (UIViewController* __nullable)theController
                 okAction:(void (^ __nullable)(void))okCallback
             cancelAction:(void (^ __nullable)(void))cancelCallback;

-(void)addShadowWithColor: (UIColor* __nullable)color;

@end
