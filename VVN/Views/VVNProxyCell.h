//
//  VVNProxyCell.h
//  Potatso
//
//  Created by Xinghou Liu on 20/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VVNProxyCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel* labelTitle;
@property(nonatomic, weak) IBOutlet UILabel* labelSubtitle;
@property(nonatomic, weak) IBOutlet UIImageView *imageViewTick;
@property(nonatomic, weak) IBOutlet UIView *viewMain;

-(void)updateUiWithTitle: (NSString*) titleText subTitle: (NSString*)subText;

@end
