//
//  UIView+VVNStyle.m
//  Potatso
//
//  Created by Xinghou.Liu on 16/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

#import "UIView+VVNStyle.h"
#import "Potatso-Swift.h"

const NSInteger     kTagSpinnerMaskView = 99223;
const NSInteger     kTagSpinnerView = 99224;
const NSInteger     kTagCommonInfoView = 7777;
const CGFloat       kCommonInfoViewWidth = 320.0f;
const CGFloat       kCommonInfoViewDisplayTimeInSecond = 3.0f;

@implementation UIView (VVNStyle)

#pragma mark - Spinner Waiting

-(void)startWaitingSpinnerOnly
{
    //add the spinner
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.tag = kTagSpinnerView;
    spinner.center = CGPointMake(self.frame.size.width/2.0f, self.frame.size.height/2.0f);
    [self addSubview: spinner];
    
    [spinner startAnimating];
}

-(void)startWaiting
{
    [self stopWaiting];
    
    //add the mask view
    UIView *maskView = [[UIView alloc] initWithFrame: self.bounds];
    maskView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    maskView.tag = kTagSpinnerMaskView;
    [self addSubview: maskView];
    maskView.clipsToBounds = YES;
    self.clipsToBounds = YES;
    
    CGFloat superWidth = self.frame.size.width;
    CGFloat superHeight = self.frame.size.height;
    if( superWidth == superHeight )
    {
        maskView.layer.cornerRadius = superHeight/2.0f;
    }
    
    //add the spinner
    [self startWaitingSpinnerOnly];
}

-(void)stopWaiting
{
    UIActivityIndicatorView *spinner = [self spinnerView];
    UIView *maskView = [self spinnerMaskView];
    
    [spinner stopWaiting];
    [maskView removeFromSuperview];
    [spinner removeFromSuperview];
}


-(UIView*)viewWithTag: (NSInteger) viewTag
{
    UIView *foundView = nil;
    for(UIView *aView in self.subviews)
    {
        if(aView.tag == viewTag)
        {
            foundView = aView;
            break;
        }
    }
    return foundView;
}
-(UIActivityIndicatorView*)spinnerView
{
    return (UIActivityIndicatorView*)[self viewWithTag: kTagSpinnerView];
}

-(UIView*) spinnerMaskView
{
    return [self viewWithTag:kTagSpinnerMaskView];
}

#pragma mark - Temp information
-(UIView*)infoView
{
    UIView *foundInfoView=nil;
    for (UIView *aSubView in self.subviews)
    {
        if ([aSubView isKindOfClass: [UILabel class]] &&
            aSubView.tag == kTagCommonInfoView)
        {
            foundInfoView = aSubView;
            break;
        }
    }
    
    return foundInfoView;
}

-(void)showTempInfo: (NSString*)infoText
{
    //remove the old info view first
    UIView* foundInfoView = [self infoView];
    if( foundInfoView != nil)
    {
        [foundInfoView removeFromSuperview];
        [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                 selector:@selector(removeInfoViewLater)
                                                   object:nil];
    }
    
    //add a new info view
    CGRect infoFrame = CGRectMake(0,
                                  0,
                                  self.bounds.size.width,
                                  64.0f);
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame: infoFrame];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent: 0.88f];
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.text = infoText;
    infoLabel.tag = kTagCommonInfoView;
    infoLabel.numberOfLines = 3;
    infoLabel.minimumScaleFactor = 0.1f;
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    infoLabel.adjustsFontSizeToFitWidth = YES;
    infoLabel.font  = [UIFont systemFontOfSize:13.0f];
    infoLabel.clipsToBounds = YES;
    //infoLabel.center = CGPointMake(self.center.x, kCommonInfoViewWidth/2+20.0f);
    
    infoLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin ;
    
    //round the view
    //[infoLabel.layer setCornerRadius:8.0f ];
    
    //add to the main view
    infoLabel.alpha = 0.0f;
    [self addSubview: infoLabel];
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         infoLabel.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(removeInfoViewLater)
                                    withObject:nil
                                    afterDelay:kCommonInfoViewDisplayTimeInSecond];
                     }];
    
    
}

-(void)removeInfoViewLater
{
    UIView* foundInfoView = [self infoView];
    
    if( foundInfoView == nil)
    {
        return;
    }
    
    [UIView animateWithDuration:0.2f
                     animations:^{
                         foundInfoView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         
                         [foundInfoView removeFromSuperview];
                     }];
}

-(void)showPrivacyAlertIn: (UIViewController*)theController
                 okAction:(void (^ __nullable)(void))okCallback
             cancelAction:(void (^ __nullable)(void))cancelCallback
{
    
    NSString *msg = @"You must have an account with ssr.vvn.io to be able to login. \n\nVVN will not collect any personal data if you are not an existing user. \n\nFor existing users, VVN will only collect email address and data usage for internal analysis. \n\nVVN will not share any user data with other entities.\n\nVVN will not collect any data content during usage of VPN";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"VVN"
                                                                    message:msg 
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", nil)
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   [alert dismissViewControllerAnimated:YES completion:^{
                                                   }];
                                                   if(okCallback)
                                                   {
                                                       okCallback();
                                                   }
                                                  
                                                  
    }];
    [alert addAction: ok];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [alert dismissViewControllerAnimated:YES
                                                                                          completion:^{}];
                                                       if(cancelCallback)
                                                       {
                                                           cancelCallback();
                                                       }
                                                   }];
    [alert addAction: cancel];
    
    [theController presentViewController:alert animated:YES completion:nil];
}

-(void)addShadowWithColor: (UIColor*)color
{
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowOffset = CGSizeMake(0.0f, 4.0f);
    self.layer.shadowRadius = 8.0f;
    self.layer.shadowOpacity = 1.0f;
    self.layer.masksToBounds = NO;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = UIScreen.mainScreen.scale;
}

@end
