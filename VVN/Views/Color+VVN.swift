//
//  Color+VVN.swift
//  Potatso
//
//  Created by Xinghou.Liu on 16/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import Foundation


extension UIColor {
    
    @objc static func vvnBackground() -> UIColor
    {
        let color = UIColor.init(red: 88.0/255.0, green: 51.0/255.0, blue: 137.0/255.0, alpha: 1.0)
        return color
    }
    
    @objc static func vvnText() -> UIColor
    {
        return UIColor.white
    }
    
    @objc static func vvnON() -> UIColor
    {
        let color = UIColor.init(red: 66.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        return color
    }
}
