//
//  VVNRequest.swift
//  Potatso
//
//  Created by Xinghou.Liu on 13/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

let vvnUrlRegister = "https://ssr.vvn.io/register"
let vvnUrlLearnMore = "https://www.vvn.io"

class VVNRequest: NSObject {
    
    static let endPointLogin = "https://api.vvn.io/ssr/login"
    static let endPointListProxy = "https://api.vvn.io/ssr/nodes"
    static let endPointUserDetails = "https://api.vvn.io/ssr/user"
    
    
    static func login(user userName :String!,
                      password pwd: String!,
                      handler: @escaping (Error?, [String:Any]?)->Void) ->Void
    {
        var request = URLRequest(url:URL(string: endPointLogin)!)
        request.httpMethod = "POST"
        
        let postString = "username=" + userName + "&password=" + pwd
        request.httpBody = postString.data(using: .ascii)
        
        let task = URLSession.shared.dataTask(with:  request) { (data, response, error) in
        
            if error != nil
            {
                handler(error!, nil)
                return;
            }
            
            // got json object
            var jsonDic: [String:Any]? = nil
            do
            {
                jsonDic = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any]
                
                if VVNUser.save(jsonDic)
                {
                    handler(nil, jsonDic)
                }
                else
                {
                    handler(NSError.init(code: 9999, description: "No user token found in response."), jsonDic)
                }
                return
            }
            catch let loginError
            {
                if let stringResponse = String.init(data: data!, encoding: .ascii)
                {
                    NSLog("Login Error: \(stringResponse)")
                    let errorToShow = NSError.init(domain: "", code: 9999, userInfo:[NSLocalizedDescriptionKey: stringResponse])
                    handler(errorToShow, nil)
                }
                else
                {
                    handler(loginError, nil)
                }
            }
        }
        
        task.resume()
        
    }
    
    static func logout(with handler: @escaping (Error?)->Void)
    {
        VVNUser.clearSession()
        handler(nil)
    }
    
    static func getUsage(handler: @escaping(Error?, VVNUsageInfo?)->Void) ->Void
    {
        VVNRequest.get(with: endPointUserDetails) { (data, response, error) in
            if error != nil
            {
                handler(error!, nil)
                return;
            }
            
            // got json object
            var usage: [String: Any]? = nil
            do
            {
                usage = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                let usageInfo = VVNUsageInfo.init(with: usage)
                VVNUser.usageInfo = usageInfo
                handler(nil, usageInfo)
                return
            }
            catch
            {
                handler(error, nil)
                return
            }
        }
    }
    
    static func getProxies(handler: @escaping(Error?, [Any]?)->Void) ->Void
    {
        VVNRequest.get(with:  endPointListProxy) { ( data, respnse, error) in
            if error != nil
            {
                handler(error!, nil)
                return;
            }
            
            // got json object
            var proxies: [Any]? = nil
            do
            {
                proxies = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [Any]
                VVNProxy.setDefaultProxyFrom(proxies)
                handler(nil, proxies)
                return
            }
            catch
            {
                handler(error, nil)
                return
            }
        }
    }
    
    static func get(with urlText: String!,
                    completion handler: @escaping (Data?, URLResponse?, Error?) -> Void )
    {
        var request = URLRequest(url:URL(string: urlText)!)
        request.httpMethod = "GET"
        
        var sendString = ""
        
        if let userToken = VVNUser.userToken()
        {
            sendString = userToken
        }
        else
        {
            return
        }
        request.setValue(sendString, forHTTPHeaderField: "token")
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            handler(data, response, error)
        }
        task.resume()
    }
    
    static func cancelAll()
    {
        URLSession.shared.invalidateAndCancel()
    }
}
