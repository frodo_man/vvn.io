//
//  VVNUserDetails.swift
//  Potatso
//
//  Created by Xinghou Liu on 25/05/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

enum VVNAccountStatus {
    case active
    case notActivated
    case forbidden
}

class VVNUsageInfo: NSObject {
    
    var totalBytes: Double? = 0.0
    var usedBytes: Double? = 0.0
    var diffBytes: Double? = 0.0
    var expiresIn: Date? = nil
    var errorMessage: String? = nil
    var accountStatus : VVNAccountStatus = .notActivated
    var validToUse: Bool = false
    
    init(with dic: [String: Any]!)
    {
        super.init()
        
        if let userInfo = dic["user"] as? [String: Any]
        {
            self.totalBytes =  userInfo["transfer_enable"] as? Double
            self.usedBytes = userInfo["u"] as? Double
            self.diffBytes = userInfo["d"] as? Double
            
            if let dateText = userInfo["expire_time"] as? String
            {
                self.expiresIn =  self.dateFrom(string: dateText)
            }
            
            self.getAccountStatus(with: userInfo)
            self.checkAccountStatus()
        }
    }
    
    func getAccountStatus(with dic: [String: Any]!)
    {
        if let rawStatus = dic["status"] as? Int
        {
            switch rawStatus
            {
            case 1:
                self.accountStatus = .active
            case -1:
                self.accountStatus = .forbidden
            default:
                self.accountStatus = .notActivated
            }
        }
    }
    
    func checkAccountStatus()
    {
        if self.accountStatus != .active
        {
            if self.accountStatus == .forbidden
            {
                self.errorMessage = "AccountError.forbidden".localized()
            }
            else if self.accountStatus == .notActivated
            {
                self.errorMessage = "AccountError.notActivated".localized()
            }
            
            self.validToUse = false
            return
        }
        else
        {
            if self.serviceExpired()
            {
                self.errorMessage = "AccountError.expired".localized()
                self.validToUse = false
            }
            
            else if self.availableDataInM() <= 0.0
            {
                self.errorMessage = "AccountError.noDataLeft".localized()
                self.validToUse = false
            }
            else
            {
                self.validToUse = true 
            }
        }
    }
    
    func totalDataInM () -> Float!
    {
        if let dbM = self.dataInM(self.totalBytes)
        {
            return Float(dbM)
        }
        return Float(0.0)
    }
    
    func availableDataInM() -> Float!
    {
        if let dbM = self.dataInM(self.totalBytes! - self.usedBytes! - self.diffBytes!)
        {
            if Float(dbM) > 0.0
            {
                return Float(dbM)
            }
        }
        return Float(0.0)
    }
    
    func availableDataRate() -> Float!
    {
        let rate = self.availableDataInM() / self.totalDataInM()
        return rate
    }
    
    func dataInM(_ byteCount: Double!) -> Double!
    {
        return byteCount / (1024.0 * 1024.0)
    }
    
    func dateFrom(string dateText: String!) -> Date?
    {
        // example of dateText:
        // 2019-03-13T00:00:00.000Z
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        if let date = dateFormatter.date(from:dateText)
        {
            return date
        }
        return nil
    }
    
    func serviceExpired() -> Bool
    {
        if let expireDate = self.expiresIn
        {
            let now = Date()
            let diff = Int(expireDate.timeIntervalSince(now))
            
            if diff <= 0
            {
                return true
            }
            return false
        }
        return false
    }
}

//MARK: - View Model
extension VVNUsageInfo
{
    func availableDataToDisplay() -> String!
    {
        // for example 2.00 G
        let textTotal = self.totalDataText()
        
        // for example 1.82 G
        let textAvailable = self.availableDataText()
        
        let finalText = "Usage.available".localized() + ": " + textAvailable! + " / " + textTotal!
        return finalText
        
    }
    
    func expireTimeToDisplay() -> String!
    {
        if let expireDate = self.expiresIn
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let dateText = dateFormatter.string(from: expireDate)
            return "Usage.expire".localized() + ": " + dateText
        }
        return ""
    }
    
    func availableDataShort() -> String!
    {
        var dataLeft = ""
    
        dataLeft = self.availableDataText()
        
        return dataLeft;
    }
    
    func expireTimeShort() -> String!
    {
        var timeText = ""
        
        if let expireDate = self.expiresIn
        {
            let now = Date()
            let hourSeconds = 3600
            let daySecconds = 3600 * 24
            
            let diff = Int(expireDate.timeIntervalSince(now))
            
            if diff > daySecconds
            {
                let dayLeft = diff / daySecconds
                timeText = "\(dayLeft)" + " " + "Days".localized()
            }
            else if diff > hourSeconds
            {
                let hourLeft = diff / hourSeconds
                timeText = "\(hourLeft)" + " " + "Hours".localized()
            }
            else if diff > 60
            {
                let minLeft = diff / 60
                timeText = "\(minLeft)" + " " + "Minutes".localized()
            }
            else if diff > 0
            {
                timeText = "\(diff)" + " " + "Seconds".localized()
            }
            else
            {
                timeText = "Expired".localized()
            }
        }
        
        return timeText
    }
    
    func totalDataText() -> String!
    {
        return self.textFor(byte: self.totalBytes)
    }
    
    func availableDataText() -> String!
    {
        var available = self.totalBytes! - self.usedBytes! - self.diffBytes!
        if available <= 0.0
        {
            available = 0.0
        }
        
        return self.textFor(byte: available)
    }
    
    func textFor(byte byteCount: Double!) -> String!
    {
        let oneM = 1024.0 * 1024.0;
        
        if byteCount > oneM
        {
            let totalM = byteCount / oneM
            
            if totalM  < 1024.0
            {
                return self.roundedDouble(totalM) + "M"
            }
            else
            {
                return  self.roundedDouble(totalM/1024.0) + "G"
            }
        }
        else
        {
            return self.roundedDouble(byteCount) + "B"
        }
    }
    
    func roundedDouble(_ aNumber: Double!) -> String!
    {
        let numString = NSString.init(format: "%.02f", aNumber)
        return numString as String 
    }
}
