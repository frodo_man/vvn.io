//
//  VVNUser.swift
//  Potatso
//
//  Created by Xinghou.Liu on 13/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

let kUserSettingsUserToken = "kUserSettingsUserToken"
let kUserSettingsTermsAccepted = "kUserSettingsTermsAccepted"

class VVNUser: NSObject {
    
    static var usageInfo: VVNUsageInfo?
    
    static func save(_ userData: [String:Any]?) -> Bool
    {
        if let userToken = userData?["token"] as? String
        {
            Potatso.sharedUserDefaults().set( userToken, forKey: kUserSettingsUserToken)
            return true
        }
        return false
    }
    
    static func clearSession()
    {
        Potatso.sharedUserDefaults().set( nil, forKey: kUserSettingsUserToken)
    }
    
    static func userToken()->String?
    {
        if let token = Potatso.sharedUserDefaults().value(forKey:kUserSettingsUserToken) as? String
        {
            return token
        }
        return nil
    }
    
    static func termsAccepted()->Bool
    {
        if let num = Potatso.sharedUserDefaults().value(forKey: kUserSettingsTermsAccepted) as? NSNumber
        {
            return num.boolValue
        }
        return false
    }
    
    static func acceptTerms()
    {
        Potatso.sharedUserDefaults().set(NSNumber(value: true), forKey: kUserSettingsTermsAccepted)
    }
    
    static func declineTerms()
    {
        Potatso.sharedUserDefaults().set(NSNumber(value: false), forKey: kUserSettingsTermsAccepted)
    }
}
