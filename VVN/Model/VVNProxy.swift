//
//  VVNProxy.swift
//  Potatso
//
//  Created by Xinghou Liu on 14/04/2018.
//  Copyright © 2018 TouchingApp. All rights reserved.
//

import UIKit

let kUserSettingsVVNProxy = "kUserSettingsVVNProxy"


class VVNProxy: NSObject {
    
    static func setDefaultPrxyFirstTime(_ aProxy: Proxy!)
    {
        let defaultProxy = Potatso.sharedUserDefaults().object(forKey: kUserSettingsVVNProxy)
        
        if defaultProxy != nil
        {
            return
        }
        else
        {
           VVNProxy.setDefaultProxyObject(with:  aProxy)
        }
    }
    
    static func setDefaultProxyFrom(_ proxies: [Any]!)
    {
        if proxies.count <= 0
        {
            return
        }
        let defaultProxy = Potatso.sharedUserDefaults().object(forKey: kUserSettingsVVNProxy)
        
        if defaultProxy != nil
        {
            return
        }
        
        if let proxyDic = proxies[0] as? [String: String]
        {
            /* example:
             Proxy: [
             -"obfsparam": "cloudfront.com",
             "method": "chacha20",
             -"protocol": "auth_aes128_md5",
             -"server": "tw.vvn.io",
             -"password": "vvn",
             -"server_port": "443",
             "protocolparam": "12418:i3wmkZ8Y",
             "remarks": "VVN-TW",
             -"obfs": "tls1.2_ticket_auth"]
             
             */
            VVNProxy.setDefaultProxy(proxyDic)
        }
    }
    
    static func setDefaultProxy( _ proxyData: [String:Any]!)
    {
        Potatso.sharedUserDefaults().set( proxyData, forKey: kUserSettingsVVNProxy)
    }
    
    static func setDefaultProxyObject(with aProxy: Proxy!)
    {
        let proxyDic = VVNProxy.dictionary(from: aProxy)
        VVNProxy.setDefaultProxy(proxyDic)
    }
    
    static func defaultProxyObject() -> Proxy?
    {
        if let proxyData = Potatso.sharedUserDefaults().object(forKey: kUserSettingsVVNProxy) as? [String:Any]
        {
            let proxyObject = VVNProxy.proxyObject(from: proxyData)
            return proxyObject
        }
        return nil
    }
    
    static func vvnProxy()-> [String:String]?
    {
        var proxyData: [String:String]? = nil
        proxyData = Potatso.sharedUserDefaults().object(forKey:  kUserSettingsVVNProxy) as? [String : String]
        return proxyData
    }
    
    static func proxyObject(from oneProxy: [String: Any] )->Proxy!
    {
        let upstreamProxy = Proxy()
        
        upstreamProxy.type = ProxyType.ShadowsocksR
        upstreamProxy.name = (oneProxy["remarks"] as? String) ?? ""
        upstreamProxy.host = (oneProxy["server"] as? String) ?? ""
        upstreamProxy.port = (oneProxy["server_port"] as? Int) ?? 443
        upstreamProxy.authscheme = (oneProxy["method"] as? String) ?? ""
        //upstreamProxy.user = user
        upstreamProxy.password = (oneProxy["password"] as? String) ?? ""
        upstreamProxy.ota = true
        upstreamProxy.ssrProtocol = (oneProxy["protocol"] as? String) ?? ""
        upstreamProxy.ssrObfs = (oneProxy["obfs"] as? String) ?? ""
        upstreamProxy.ssrObfsParam = (oneProxy["obfsparam"] as? String) ?? ""
        upstreamProxy.ssrProtocolParam = (oneProxy["protocolparam"] as? String) ?? ""
        
        return upstreamProxy
    }
    
    static func dictionary(from proxy: Proxy!) -> [String: Any]!
    {
        let proxyDic = NSMutableDictionary()
        
        proxyDic["remarks"] = proxy.name
        proxyDic["server"] = proxy.host
        proxyDic["server_port"] = proxy.port
        proxyDic["method"] = proxy.authscheme
        proxyDic["password"] = proxy.password
        proxyDic["protocol"] = proxy.ssrProtocol
        proxyDic["obfs"] = proxy.ssrObfs
        proxyDic["obfsparam"] = proxy.ssrObfsParam
        proxyDic["protocolparam"] = proxy.ssrProtocolParam
        
        return proxyDic as! [String: Any]
    }
    
    //MARK: - DB
    static func deleteAllProxiesInDB()
    {
        // remove all from the data base
        let rmObjects = defaultRealm.objects(Proxy.self)
        if rmObjects.count > 0
        {
            try! defaultRealm.write {
                defaultRealm.delete(rmObjects)
            }
        }
    }
    
    static func saveAll(proxies proxyArray: [Any]!)
    {
        for oneProxy in proxyArray!
        {
            if let proxyDic = oneProxy as? [String: Any]
            {
                NSLog("Proxy: \(proxyDic)")
                VVNProxy.saveOneProxy(proxyDic)
            }
        }
    }
    
    static func saveOneProxy( _ oneProxy:[String: Any]!)
    {
        let upstreamProxy = Proxy()
        
        do{
            upstreamProxy.type = ProxyType.ShadowsocksR
            upstreamProxy.name = (oneProxy["remarks"] as? String) ?? ""
            upstreamProxy.host = (oneProxy["server"] as? String) ?? ""
            upstreamProxy.port = (oneProxy["server_port"] as? Int) ?? 443
            upstreamProxy.authscheme = (oneProxy["method"] as? String) ?? ""
            upstreamProxy.password = (oneProxy["password"] as? String) ?? ""
            upstreamProxy.ota = true
            upstreamProxy.ssrProtocol = (oneProxy["protocol"] as? String) ?? ""
            upstreamProxy.ssrObfs = (oneProxy["obfs"] as? String) ?? ""
            upstreamProxy.ssrObfsParam = (oneProxy["obfsparam"] as? String) ?? ""
            upstreamProxy.ssrProtocolParam = (oneProxy["protocolparam"] as? String) ?? ""
            
            try DBUtils.add(upstreamProxy)
            
        }
        catch let error
        {
            print("Failed to save proxy to server \(error)")
        }
    }
}
